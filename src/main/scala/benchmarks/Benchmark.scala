package benchmarks

import annotation.tailrec
import com.google.caliper.Param

object Maps {

  import scala.collection.JavaConversions._
  import com.hazelcast.core.Hazelcast
  import com.hazelcast.config.{Config => HazelcastConfig, MapConfig}
  import org.infinispan.Cache
  import org.infinispan.manager.DefaultCacheManager

  val hazelcastCache:java.util.Map[String, String] = {
    val config = new HazelcastConfig()
    config.getMapConfig("default").setInMemoryFormat(MapConfig.DEFAULT_IN_MEMORY_FORMAT);
    Hazelcast newHazelcastInstance(config) getMap "example"
  }

  val infinispanCache:java.util.Map[String, String] = new DefaultCacheManager getCache "example"

  import scala.util.hashing.MurmurHash3

  def uuid(i:Int) = new java.util.UUID(0, MurmurHash3.stringHash(i.toString)).toString
}

class Benchmark extends SimpleScalaBenchmark {

  import java.util.UUID
  import scala.collection.mutable.MutableList

//  @Param(Array("10", "100", "1000"))
  @Param(Array("1000", "10000", "100000"))
  val length: Int = 0

  @Param(Array("Infinispan", "Hazelcast"))
  val service:String = ""
  var cache:java.util.Map[String, String] = _

  val ids = MutableList[String]()

  val lorem = """
    Sed blandit augue non augue cursus sodales. Morbi suscipit arcu vitae nulla porta eleifend. Fusce scelerisque faucibus mi tempus suscipit.
    Praesent dignissim egestas convallis. Praesent tellus urna, lobortis nec pellentesque vel, venenatis placerat lorem. Fusce cursus fermentum
    tempor. Curabitur convallis est id urna faucibus id ullamcorper massa porta. Morbi id iaculis orci. Fusce pellentesque lectus dui, non
    pellentesque lectus. Maecenas ornare justo vel elit dapibus et tincidunt eros aliquam. Curabitur imperdiet eros vitae arcu euismod sed
    dictum dui sollicitudin. Nam consectetur pretium dolor, sit amet tempor ligula ultricies a. Suspendisse sem diam, vulputate at tincidunt eu,
    aliquam sit amet purus. Nam facilisis, neque id pharetra euismod, diam velit aliquet lacus, elementum tincidunt arcu turpis at metus.
    Suspendisse egestas diam a libero placerat sed pharetra risus cursus. Fusce viverra orci ut ipsum elementum nec commodo eros viverra.
  """

  override def setUp() {
    cache = service match {
      case "Infinispan" => Maps.infinispanCache
      case "Hazelcast" => Maps.hazelcastCache
    }

    for(i <- 0 to length)
      ids += Maps.uuid(i)
  }

  def timeSet(reps:Int) = repeat(reps) {
    for(uuid <- ids)
      cache.put(uuid, lorem + uuid)

    cache
  }

  def timeGet(reps:Int) = repeat(reps) {
    for(uuid <- ids)
      cache.get(uuid)

    cache
  }

  override def tearDown() {
    // Hazelcast.shutdownAll
  }

}

# Map vs Hazelcast vs Infinispan

## Overview

This is just a simple benchmark to compare [Hazelcast](http://www.hazelcast.com) and [Infinispan](http://www.jboss.org/infinispan/) performance (with defaults), and includes a regular Map object as a baseline.

### Hazelcast

An (optionally) persistent Key/Value store with strong availability guarantees.

If you value Durability, Consistency, and High Availability in an Embedded K/V store then Hazelcast might be what you're looking for. It's worth mentioning that Hazelcast should also scale horizontally very well.

Hazelcast only requires a single JAR, which makes it very easy to get started with.

### Infinispan

Intended to be a straight up cache, compatible with the MemCache API. It's data set is in memory, supports locality (Hazelcast's NearCache) and a more advanced eviction algorithm (LIRS; no idea how much this contributes to overall performance really).

Infinispan should also scale out horizontally very well as Maps can be "sharded", nodes can be rack aware (through configuration) and keys locally cached on access.

Infinispan requires nine JARs to run an embedded instance, which is kind of the opposite of "impressive".

## Usage

From the root of the project run:

```bash
sbt run
```

## Results

On an iMac (i7 @ 3.4GHz) these are my results (for Hazelcast 2.5):

```
[info]    service length benchmark        us linear runtime
[info] Infinispan   1000       Set    1293.5 =
[info] Infinispan   1000       Get      76.8 =
[info] Infinispan  10000       Set   15148.0 =
[info] Infinispan  10000       Get     769.5 =
[info] Infinispan 100000       Set  260027.8 =====
[info] Infinispan 100000       Get    7076.2 =
[info]  Hazelcast   1000       Set   14175.7 =
[info]  Hazelcast   1000       Get    9796.7 =
[info]  Hazelcast  10000       Set  145664.5 ==
[info]  Hazelcast  10000       Get   97930.3 =
[info]  Hazelcast 100000       Set 1557023.0 ==============================
[info]  Hazelcast 100000       Get 1010993.5 ===================
```

Same machine for Hazelcast 3.1 (default configuration):

```
[info]    service length benchmark        us linear runtime
[info] Infinispan   1000       Set    1299.4 =
[info] Infinispan   1000       Get      76.4 =
[info] Infinispan  10000       Set   15604.5 =
[info] Infinispan  10000       Get     768.2 =
[info] Infinispan 100000       Set  253003.0 ====
[info] Infinispan 100000       Get    6362.6 =
[info]  Hazelcast   1000       Set   16330.0 =
[info]  Hazelcast   1000       Get    9177.0 =
[info]  Hazelcast  10000       Set  168105.1 ===
[info]  Hazelcast  10000       Get   93608.2 =
[info]  Hazelcast 100000       Set 1670595.0 ==============================
[info]  Hazelcast 100000       Get  952421.0 =================
```